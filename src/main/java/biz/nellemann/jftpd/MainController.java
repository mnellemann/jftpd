package biz.nellemann.jftpd;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.filesystem.nativefs.NativeFileSystemFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.ssl.SslConfigurationFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Objects;

public class MainController {

    private final static Logger log = LoggerFactory.getLogger(MainController.class);

    private FtpServer ftpServer;
    private String userHomeDirectory;


    @FXML
    private BorderPane borderPane;

    @FXML
    private Spinner<Integer> intPort;

    @FXML
    private Label labelHomeDirectory;

    @FXML
    private TextField usernameString;

    @FXML
    private TextField passwordString;

    @FXML
    private ComboBox<ServerSecurity> securityCombo;

    @FXML
    private Button btnStart;

    @FXML
    private Button btnStop;

    @FXML
    public TextArea textAreaLog;


    @FXML
    public void initialize() {
        PrintStream printStream = new PrintStream(new MyCustomAppender(textAreaLog));
        System.setOut(printStream);
        System.setErr(printStream);

        userHomeDirectory = System.getProperty("java.io.tmpdir");
        labelHomeDirectory.setText(userHomeDirectory);

        securityCombo.getItems().setAll(ServerSecurity.values());
        securityCombo.getSelectionModel().select(0);
    }


    @FXML
    protected void onSelectButtonClick() {

        Stage stage = (Stage) borderPane.getScene().getWindow();
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(stage);

        if(selectedDirectory != null){
            userHomeDirectory = selectedDirectory.getAbsolutePath();
            labelHomeDirectory.setText(userHomeDirectory);
        }
    }


    @FXML
    protected void onStartButtonClick() {
        if(userHomeDirectory == null || userHomeDirectory.isEmpty()) {
            log.warn("No local folder selected.");
            return;
        }

        textAreaLog.setText("");
        Platform.runLater(() -> {
            try {
                startServer();
                btnStart.disableProperty().setValue(true);
                btnStop.disableProperty().setValue(false);
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        });
    }


    @FXML
    protected void onStopButtonClick() {
        Platform.runLater(() -> {
            btnStart.disableProperty().setValue(false);
            btnStop.disableProperty().setValue(true);
            stopServer();
        });
    }


    private void startServer() throws Exception {

        try {
            MyCustomUser user = new MyCustomUser(usernameString.getText(), passwordString.getText(), userHomeDirectory);
            MyCustomUserManager myCustomUserManager = new MyCustomUserManager();
            myCustomUserManager.save(user);

            ListenerFactory factory = getListenerFactory();
            FtpServerFactory serverFactory = new FtpServerFactory();
            serverFactory.setUserManager(myCustomUserManager);
            serverFactory.setFileSystem(new NativeFileSystemFactory());
            serverFactory.addListener("default", factory.createListener());

            ftpServer = serverFactory.createServer();
            ftpServer.start();
        } catch (FtpException ex) {
            throw new Exception(ex.getMessage());
        }
    }


    private void stopServer() {
        try {
            ftpServer.stop();
        } catch (Exception e) {
            log.warn("stopServer() - {}", e.getMessage());
        }
    }


    private ListenerFactory getListenerFactory() {
        ListenerFactory factory = new ListenerFactory();
        factory.setPort(intPort.getValue());    // set the port of the listener

        if(!securityCombo.getSelectionModel().getSelectedItem().equals(ServerSecurity.NONE)) {
            // keytool -genkey -keyalg RSA -alias jftpd -keystore keystore.jks -validity 3650 -keysize 2048
            try {
                SslConfigurationFactory ssl = new SslConfigurationFactory();
                File keystoreFile = new File(getClass().getResource("/keystore.jks").toURI());
                ssl.setKeystoreFile(keystoreFile);
                ssl.setKeystorePassword("password");
                ssl.setTruststoreFile(null);

                factory.setSslConfiguration(ssl.createSslConfiguration());
                if(securityCombo.getSelectionModel().getSelectedItem().equals(ServerSecurity.IMPLICIT)) {
                    factory.setImplicitSsl(true);
                }
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }

        }
        return factory;
    }



}
