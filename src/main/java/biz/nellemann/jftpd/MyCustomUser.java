package biz.nellemann.jftpd;

import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.AuthorizationRequest;
import org.apache.ftpserver.ftplet.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MyCustomUser implements User {

    private final static Logger log = LoggerFactory.getLogger(MyCustomUser.class);

    private final String username;
    private final String password;
    private final String homeDirectory;
    private boolean enabled = true;
    private int maxIdleTime = 3600;

    MyCustomUser(String username) {
        this(username, username);
    }

    MyCustomUser(String username, String password) {
        this(username, password, System.getProperty("java.io.tmpdir"));
    }

    MyCustomUser(String username, String password, String homeDirectory) {
        this.username = username;
        this.password = password;
        this.homeDirectory = homeDirectory;
    }

    @Override
    public String getName() {
        log.debug("getName()");
        return username;
    }

    @Override
    public String getPassword() {
        log.debug("getPassword()");
        return password;
    }

    @Override
    public List<? extends Authority> getAuthorities() {
        log.debug("getAuthorities()");
        return null;
    }

    @Override
    public List<? extends Authority> getAuthorities(Class<? extends Authority> clazz) {
        log.debug("getAuthorities(Class)");
        return null;
    }

    @Override
    public AuthorizationRequest authorize(AuthorizationRequest request) {
        log.debug("authorize() request {}", request.toString());
        // TODO: Check request ?  ConcurrentLoginRequest, WriteRequest, TransferRateRequest....
        return request;
    }

    @Override
    public int getMaxIdleTime() {
        return maxIdleTime;
    }

    @Override
    public boolean getEnabled() {
        return enabled;
    }

    @Override
    public String getHomeDirectory() {
        log.debug("getHomeDirectory()");
        return homeDirectory;
    }
}
