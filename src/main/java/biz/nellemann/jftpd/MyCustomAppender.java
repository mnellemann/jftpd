package biz.nellemann.jftpd;

import javafx.application.Platform;
import javafx.scene.control.TextArea;
import java.io.IOException;
import java.io.OutputStream;

public class MyCustomAppender extends OutputStream {

    private final TextArea textArea;


    public MyCustomAppender(TextArea textArea) {
        this.textArea = textArea;
    }


    @Override
    public void write(int b) {
        Platform.runLater( () -> {
            textArea.appendText(String.valueOf((char) b));
        });
    }

}
