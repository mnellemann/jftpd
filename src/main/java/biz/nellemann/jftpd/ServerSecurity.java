package biz.nellemann.jftpd;

public enum ServerSecurity {
    NONE,
    IMPLICIT,
    EXPLICIT
}
