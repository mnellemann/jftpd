package biz.nellemann.jftpd;

import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.usermanager.AnonymousAuthentication;
import org.apache.ftpserver.usermanager.ClearTextPasswordEncryptor;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class MyCustomUserManager implements UserManager {

    private final static Logger log = LoggerFactory.getLogger(MyCustomUserManager.class);

    private final Map<String, User> users = new HashMap<>() {
        {
            //put("anonymous", new MyCustomUser("anonymous"));
            //put("admin", new MyCustomUser("admin"));
            //put("test", new MyCustomUser("test"));
        }
    };


    @Override
    public User getUserByName(String username) {
        return users.get(username);
    }


    @Override
    public String[] getAllUserNames() throws FtpException {
        return users.keySet().toArray(new String[0]);
    }


    @Override
    public void delete(String username) throws FtpException {
        users.remove(username);
    }


    @Override
    public void save(User user) throws FtpException {
        log.debug("save()");
        users.put(user.getName(), user);
    }


    @Override
    public boolean doesExist(String username) {
        return users.containsKey(username);
    }


    @Override
    public User authenticate(Authentication authentication) throws AuthenticationFailedException {

        log.debug("authenticate()");

        if (authentication instanceof UsernamePasswordAuthentication) {
            UsernamePasswordAuthentication upauth = (UsernamePasswordAuthentication) authentication;

            String username = upauth.getUsername();
            String password = upauth.getPassword();

            if (username == null) {
                throw new AuthenticationFailedException("Authentication failed");
            }

            if (password == null) {
                password = "";
            }

            User user = getUserByName(username);
            if(user.getPassword().matches(password)) {
                return user;
            } else {
                throw new AuthenticationFailedException("Authentication failed");
            }

        } else if (authentication instanceof AnonymousAuthentication) {
            if (doesExist("anonymous")) {
                return getUserByName("anonymous");
            } else {
                throw new AuthenticationFailedException("Authentication failed");
            }
        } else {
            throw new IllegalArgumentException(
                    "Authentication not supported by this user manager");
        }

        /*
        if(authentication instanceof AnonymousAuthentication) {
            return new MyCustomUser("anonymous");
        } else if (authentication instanceof ClearTextPasswordEncryptor) {
            return new MyCustomUser("user");
        } else {
            throw new AuthenticationFailedException();
        }

         */
    }


    @Override
    public String getAdminName() throws FtpException {
        return null;
    }

    @Override
    public boolean isAdmin(String username) throws FtpException {
        return false;
    }

}
