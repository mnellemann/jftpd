module biz.nellemann.jftpd {
    requires java.desktop;
    requires org.slf4j;
    requires org.slf4j.simple;
    requires ftplet.api;
    requires ftpserver.core;
    requires org.kordamp.ikonli.core;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.ikonli.fontawesome5;
    requires atlantafx.base;
    requires javafx.fxml;
    requires org.bouncycastle.provider;
    requires org.bouncycastle.pkix;

    opens biz.nellemann.jftpd to javafx.fxml;
    exports biz.nellemann.jftpd;
}
